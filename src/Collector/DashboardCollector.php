<?php

namespace Intriro\DevelopmentBundle\Collector;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

class DashboardCollector extends DataCollector
{
    /**
     * @var array
     */
    private $entries;

    /**
     * @param array $entries
     */
    public function __construct(array $entries) {
        $this->entries = $entries;
    }

    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $this->data = array(
            'dashboard_entries' => $this->entries,
        );
    }

    public function reset()
    {
        $this->data = [];
    }

    public function getDashboardEntries()
    {
        return $this->data['dashboard_entries'];
    }

    public function getName()
    {
        return 'intriro_dashboard';
    }
}
