<?php

namespace Intriro\DevelopmentBundle\DependencyInjection;

use Intriro\DevelopmentBundle\Collector\DashboardCollector;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class IntriroDevelopmentExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $entries = $config['wdt'];

        $dashboardDefinition = new Definition(DashboardCollector::class, [$entries]);
        $dashboardDefinition->addTag('data_collector', [
            'template' => "IntriroDevelopmentBundle::collector.html.twig",
            'id' => "intriro_dashboard",
        ]);

        $container->setDefinition('intriro_development.collector.dashboard_collector', $dashboardDefinition);
    }
}
